        
        

       


        $(".option-image").click(function(){
            var className= $(this).attr("class");
            if (className.indexOf("active")==-1){
                $(".active").removeClass("active");
                $(this).addClass("active");
                
            }
            else{
                $(this).removeClass("active");
            }

            if ($(".active").length==0){
                $("#next").addClass("disabled");
            }
            else{
                $("#next").removeClass("disabled");
            }
        });
        var score=0;
        
        var currentQuestion = 0;
        function prepareQuestion(question){
            $(".questionNumber").html(questions.indexOf(question)+1);
            $("#question").html(question.title);
            $("#image1").attr("src","./img/"+question.images[0]);
            $("#image2").attr("src","./img/"+question.images[1]);
            $("#image3").attr("src","./img/"+question.images[2]);
            $("#image4").attr("src","./img/"+question.images[3]);
            $("#next").addClass("disabled");
            $("#next").show();
            currentQuestion++;
            $("#bar").css("width",currentQuestion/questions.length*100+"%");
        }

        $(document).ready(function(){
            $(".questionCount").html(questions.length);
            $("#retry").click(function(e){
                location.reload();
            });
            prepareQuestion(questions[0]);
            
            $("#next").click(function(e){
                
                if ($(".active").length==0){
                    return;
                }
                $("#next").hide();
                $("#card").fadeOut(1000,function(){
                   // $("#next").addClass("disabled");
                    if ($(".active").attr("id")=="image"+questions[currentQuestion-1].answer){
                        score+=1;
                    }
                    if (currentQuestion<questions.length){
                        prepareQuestion(questions[currentQuestion]);
                        $(".active").removeClass("active");
                        $("#card").fadeIn(1000);
                        $(window).scrollTop($(".main-content"));
                        
                    }
                    else{
                        if (score>questions.length){
                            score=questions.length;
                        }
                        
                        $("#score").html(score);
                        $("#total").html(questions.length);
                        $("#results").fadeIn(1000);
                        $(window).scrollTop();
                    }
                })
            });
        });
        