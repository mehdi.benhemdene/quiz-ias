var questions = [
    {
        title : "Which one is the picture of Isaac Newton ? ",
        images : [
            "newton.jpg",
            "gallileo.jpg",
            "descartes.jpg",
            "gottfried.jpg"
        ],
        answer: 1
    },
    {
        title : "Which one is the picture of Albert Einstein ? ",
        images : [
            "faraday.jpg",
            "einstein.jpg",
            "maxwell.jpg",
            "thomson.jpg"
        ],
        answer: 2
    },
    {
        title : "Which one is the picture of Nikola Tesla ? ",
        images : [
            "nicolaus.jpg",
            "bernoulli.jpg",
            "volta.jpg",
            "tesla.jpeg"
        ],
        answer: 4
    },
    {
        title : "Which one is the picture of Galileo Galilei ?",
        images : [
            "volta.jpg",
            "gallileo.jpg",
            "faraday.jpg",
            "nicolaus.jpg"
        ],
        answer: 2
    },
    {
        title : "Which one is the picture of Leonardo da Vinci ? ",
        images : [
            "newton.jpg",
            "descartes.jpg",
            "volta.jpg",
            "davinci.jpg"
        ],
        answer: 4
    },
    {
        title : "Which one is the picture of Charles Darwin ?",
        images : [
            "planck.jpg",
            "thomson.jpg",
            "darwin.jpg",
            "mach.jpg"
        ],
        answer: 3
    },
    {
        title : "Which one is the picture of Max Planck ?",
        images : [
            "planck.jpg",
            "heiseinberg.jpg",
            "boltzman.jpg",
            "gallileo.jpg"
        ],
        answer: 1
    },
    {
        title : "Which one is the picture of Niels Bohr? ",
        images : [
            "joule.jpg",
            "zeeman.jpg",
            "bohr.jpg",
            "wilhelm.jpg"
        ],
        answer: 3
    },
    {
        title : "Which one is the picture of Alan Turing ?",
        images : [
            "feynman.jpg",
            "turing.jpg",
            "hawking.jpg",
            "bardeen.jpg"
        ],
        answer: 2
    },
    {
        title : "Which one is the picture of Marie Curie?",
        images : [
            "franklin.jpg",
            "meitner.jpg",
            "curie.jpg",
            "noether.jpg"
        ],
        answer: 3
    },

]
